import XCTest
@testable import SwiftDTApi

final class SwiftDTApiTests: XCTestCase {
    let api = SwiftDTApi()
    func testExample() throws {
        api.add("/api/site/list", method: .post, responseHeader: [
            "Access-Control-Allow-Origin": "*"
        ]) { request in
            print(request.params)
            print(request.headers)
            return DTApiResponse(data: "{\"a\":\"b\"}".data(using: .utf8) ?? Data())
        }
        api.start(8888)
    }
}
