//
//  File.swift
//  
//
//  Created by steve on 2024/3/7.
//

import Foundation

public struct DTApiResponse {
    /// 返回的数据
    public var data:Data = Data()
    
    /// 数据类型
    public var contentType: String = "application/json;charset=UTF-8"
    
    public init(data: Data, contentType: String = "application/json;charset=UTF-8") {
        self.data = data
        self.contentType = contentType
    }
}
