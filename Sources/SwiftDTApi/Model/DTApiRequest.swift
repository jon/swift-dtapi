//
//  File.swift
//  
//
//  Created by steve on 2024/3/7.
//

import Foundation

public struct DTApiRequest {
    /// 请求的参数
    public var params: [String: Any] = [:]
    /// 请求的 headers
    public var headers:[String: String] = [:]
    
    init(params: [String : Any], headers: [String: String]) {
        self.params = params
        self.headers = headers
    }
}
