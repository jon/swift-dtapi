//
//  File.swift
//  
//
//  Created by steve on 2024/3/7.
//

import Foundation

extension URL {
    /// 获取请求参数
    func getQueryParameters() -> [String: Any] {
        var queryParameters: [String: Any] = [:]
        
        if let components = URLComponents(url: self, resolvingAgainstBaseURL: false),
           let queryItems = components.queryItems {
            for item in queryItems {
                queryParameters[item.name] = item.value ?? ""
            }
        }
        
        return queryParameters
    }
}
